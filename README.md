# pindex

Pindex helps you identify obfuscated integrated circuits by searching through KiCad's symbols library components that matches the passed command line arguments.


## Requirements

- [Racket](https://racket-lang.org/) (tested on version 8.4)
- [KiCad Symbols](https://gitlab.com/kicad/libraries/kicad-symbols/)


## Compile and usage

```
git clone https://gitlab.com/trobador/pindex.git
cd pindex
raco exe pindex.rkt
```

To search ICs with 40 pins where pin number 12 is VSS:

```
$ ./pindex -p 40 -f '(12 . "VSS")' /usr/share/kicad/symbols/MCU_*.kicad_sym
PIC16F874-XXIP: http://ww1.microchip.com/downloads/en/DeviceDoc/30292C.pdf
PIC16F15375-xP: http://ww1.microchip.com/downloads/en/DeviceDoc/PIC16LF15356-75-76-85-86-Data%20Sheet-40001866B.pdf
PIC18F4450-IP: http://ww1.microchip.com/downloads/en/DeviceDoc/39760d.pdf
...
```

Pindex also accept regular expressions in the pin name, so you can:

```
$ ./pindex -p 40 -f '(10 . #px"V[CD]{2}")' /usr/share/kicad/symbols/MCU_*.kicad_sym
```

