#lang scribble/manual
@require[@for-label[pindex
                    racket/base]]

@title{pindex}
@author{Brian Gomes Bascoy}

@defmodule[pindex]

Component identification tool
